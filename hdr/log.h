#ifndef LOG_GUARD
#define LOG_GUARD

/*
 * Questa funzione scrive nel FILE f, passato tramite puntatore, una stringa
 * recante il messaggio di generazione di una nuova persona nel sistema.
 */
void floor_log_update(FILE *f, struct person p);

/*
 * Questa funzione scrive nel FILE f, passato tramite puntatore, una stringa
 * che comunica la fermata dell'ascensore a un piano, il cui indice e'
 * contenuto in loc.
 */
void stop_log_update(FILE *f, int loc);

/*
 * Questa funzione scrive nel FILE f, passato tramite puntatore, il messaggio
 * di caricamento di una nuova persona nell'ascensore.
 */
void load_log_update(FILE *f, struct person p, int loc);

/*
 * Questa funzione scrive nel FILE f, passato tramite puntatore, il messaggio
 * che comunica che una persona e' scesa dall'ascensore. Il parametro
 * weight serve a identificare il tipo di persona.
 */
void deload_log_update(FILE *f, enum weight weight, int loc);

/*
 * Questa funzione scrive nel FILE f, passato tramite puntatore, una stringa
 * contenente il messaggio finale da stampare al termine dell'esecuzione
 * dell'applicazione. Il parametro people e' un array contenente le quantita'
 * di ogni tipo di persona trasportate. Nella prima i bambini, poi gli adulti,
 * addetti alla consegna e addetti alla manutenzione.
 */
void final_log_print(FILE *f, unsigned int people[4]);

#endif
