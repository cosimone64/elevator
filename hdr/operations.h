#ifndef OPERATIONS_GUARD
#define OPERATIONS_GUARD

/*
 * Questa funzione genera una nuova istanza di Person. Riceve come parametri
 * w, enumerativo di tipo Weight, e un intero, dest, che identifica la
 * destinazione. Dopo aver allocato l'area di memoria desiderata tramite
 * malloc(), essa viene restituita.
 */
struct person init_person(enum weight w, int dest);

/*
 * Questa funzione restituisce, un numero pseudocasuale compreso tra
 * 0 e 3, entrambi gli estremi inclusi.
 */
int randomize_destination(void);

/*
 * Questa funzione controlla il corretto funzionamento della funzione
 * read(). In caso di fallimento, termina il programma, altrimenti restituisce
 * il valore restituito da read().
 */
int safe_read(int fd, char *buffer, int count);

/*
 * Allo stesso modo della precedente, questa funzione controlla la funzione
 * write().
 */
int safe_write(int fd, char *buffer, int count);

#endif
