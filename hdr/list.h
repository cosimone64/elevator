#ifndef LIST_GUARD
#define LIST_GUARD

/*
 * Questa struttura rappresenta una singola lista. E' formata da:
 * una persona, di tipo struct person,
 * due puntatori a struct list_node, next e previous, che puntano
 * rispettivamente all'elemento successivo e precedente.
 */
struct list_node
{
	struct person person;
	struct list_node *next;
	struct list_node *previous;
};

/*
 * Questa struttura rappresenta la coda effettiva di persone. E' formata
 * da due sottoliste di tipo struct list_node, memorizzate tramite puntatori.
 * La prima salva i dati degli addetti alla manutenzione, che hanno priorita',
 * la seconda le informazioni relative a tutte le altre persone. Le operazioni
 * di inserimento e cancellazione, infatti, daranno priorita' alla prima.
 */
struct full_list
{
	struct list_node *priority_list;
	struct list_node *normal_list;
	unsigned int number_of_nodes;
};


/*
 * Questa funzione genera una nuova istanza di struct full_list.
 * Alloca la quantita' di memoria necessaria tramite malloc()
 * e restituisce il puntatore a tale area di memoria
 */
struct full_list *init_list(void);

/*
 * Questa funzione rimuove il primo elemento della struct full_list passata
 * tramite puntatore. Restituisce la persona contenuta nel nodo rimosso.
 */
struct person remove_first(struct full_list *l);

/*
 * Questa funzione rimuove dalla lista head, passata tramite doppio puntatore,
 * l'elemento n. Il passaggio tramite doppio puntatore e' necessario per
 * modificare il riferimento al primo elemento della lista, qualora l'elemento
 * da rimuovere fosse il primo della struttura.
 */
struct person remove_local(struct list_node *n, struct list_node **head);

/*
 * Questa funzione restituisce la persona contenuta nel primo elemento della
 * lista l, passata tramite puntatore, senza pero' rimuovere il nodo.
 */
struct person *peek_list(struct full_list *l);

/*
 * Questa funzione inserisce in fondo alla lista l, passata
 * tramite puntatore, un nuovo nodo contenente la persona p.
 */
void insert_last(struct full_list *l, struct person p);

/*
 * Questa funzione dealloca l'area di meoria puntata dal puntatore
 * a struct full list l, passato come parametro.
 */
void destroy_list(struct full_list *l);

#endif
