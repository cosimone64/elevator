#ifndef TYPES_GUARD
#define TYPES_GUARD

/*
 * Viene definito un enumerativo per far si' che non possano
 * esserci valori non validi per il peso.
 */
enum weight
{
	CHILD_WEIGHT = 40,
	ADULT_WEIGHT = 80,
	CARRIER_WEIGHT = 90,
	MAINT_WEIGHT = 100
};

/*
 * Ogni persona e' completamente identificata dal suo peso e dalla
 * sua destinazione.
 */
struct person
{
	enum weight weight;
	unsigned int destination;
};

#endif
