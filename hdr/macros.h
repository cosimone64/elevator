#ifndef MACROS_GUARD
#define MACROS_GUARD

/*
 * Varie macro per facilitare la lettura.
 */
#define CORRECT_ARGUMENT "durataapplicazione="
#define DEFAULT_PROTOCOL 0
#define MAX_WEIGHT 400
#define SOCKET_NAME "../socket"

/*
 * Macro che identificano le frequenze di generazione delle persone
 * nei piani.
 */
#define CHILD_FREQ_GF 10
#define ADULT_FREQ_GF 15
#define CARRIER_FREQ_GF 60
#define MAINT_FREQ_GF 40

#define CHILD_FREQ_F1 80
#define ADULT_FREQ_F1 70
#define CARRIER_FREQ_F1 120
#define MAINT_FREQ_F1 120

#define CHILD_FREQ_F2 10
#define ADULT_FREQ_F2 15
#define CARRIER_FREQ_F2 30
#define MAINT_FREQ_F2 100

#define CHILD_FREQ_F3 40
#define ADULT_FREQ_F3 70
#define CARRIER_FREQ_F3 120
#define MAINT_FREQ_F3 40

#endif
