#ifndef FLOOR_GUARD
#define FLOOR_GUARD

/*
 * Questa struttura contiene tutti i dati necessari all'esecuzione del thread
 * generatore nei processi piano e alla comunicazione con l'ascensore. Servono:
 * il riferimento alla lista da aggiornare,
 * il riferimento al file su cui scrivere,
 * il socket su cui comunicare,
 * un array di 4 interi contenente le frequenze di generazione
 * delle persone, (in ordine bambino, adulto, addetti alla consegna
 * e addetti alla manutenzione)
 * il numero del piano, e
 * un puntatore al semaforo su cui sincronizzarsi.
 */
struct floor_comm_struct
{
	struct full_list *list;
	FILE *file;
	int socket;
	unsigned int freqs[4];
	unsigned int floor_number;
	sem_t *semaphore;
};

/*
 * Questa funzione restituisce una nuova istanza di struct floor_comm_struct.
 * Riceve come parametri un puntatore a Full_list, un puntatore a semaforo
 * e un array di 6 interi contenente le frequenze di arrivo delle persone
 * nelle prime quattro posizioni, il numero del piano nella quinta, e il
 * file descriptor del socket nella sesta. L'ultimo parametro (puntatore a
 * FILE) serve a memorizzare il file di log su cui scrivere.
 */
struct floor_comm_struct *init_comm_struct(struct full_list *list,
		sem_t *semaphore, int params[6], FILE *file);

/*
 * Questa funzione restituisce un nuovo file descriptor corrispondente
 * a un socket, attraverso il quale il piano puo' connettersi all'ascensore.
 */
int create_floor_socket(void);

/*
 * Questa funzione serve a controllare la corretta esecuzone della funzione
 * pthread_create(). In caso negativo, essa provoca l'uscita dal programma.
 * In caso affermativo, si restituisce il valore restituito
 * da pthread_create().
 */
int safe_pthread_create(pthread_t *thread, const pthread_attr_t *attr,
		void *(*start_routine) (void *), void *arg);

/*
 * Allo stesso modo, questa funzione controlla la corretta esecuzione della
 * funzione pthread_detach(). In caso affermativo, si restituisce il valore
 * restituito da pthread_detach().
 */
int safe_pthread_detach(pthread_t thread);

/*
 * Questa e' la funzione principale invocata dai piani, nella quale si svolgono
 * tutte le operazioni necessarie per la comunicazione con l'ascensore.
 * Inizialmente si legge sul socket una stringa lunga 2 * sizeof(char).
 * Se corrisponde a "no", messaggio di fine esecuzione, restituisce 0.
 * Altrimenti, si prosegue con la creazione del messaggio da scrivere
 * sul socket.Infine, viene rimosso dalla lista il numero di persone
 * comunicato dall'ascensore, e si restituisce 1.
 */
int communicate_with_elevator(struct floor_comm_struct *params);

/*
 * Questa funzione serve a "bloccare" il piano che la invoca, per attendere
 * che l'ascensore si sia connesso con tutti i piani.
 */
void wait_for_elevator(int socket, int floor_number);

/*
 * Questa funzione si occupa di generare periodicamente le persone.
 * Tramite una sleep(), essa sospende il thread generatore ogni 5 secondi.
 * Al risveglio, in base ai secondi passati dall'inizio dell'esecuzione,
 * viene generata la persona corrispondente alla frequenza desiderata.
 * Poiche' si tratta di una funzione eseguita da un thread, restituisce
 * e riceve come parametro un puntatore a void.
 */
void *generate_people(void *params);

/*
 * Questa funzione controlla la corretta esecuzione di sem_init(). In caso
 * di esito negativo, termina il programma, altrimenti restituisce il valore
 * restituito da tale funzione.
 */
void safe_sem_init(sem_t *sem, int pshared, unsigned int value);

/*
 * Allo stesso modo della precedente, questa funzione controlla il corretto
 * funzionamento di sem_wait().
 */
void safe_sem_wait(sem_t *semaphore);

/*
 * Cosi' come questa, per sem_post()
 */
void safe_sem_post(sem_t *semaphore);

/*
 * Questa funzione dealloca l'area di memoria memorizzata dal puntatore a
 * struct floor_comm_struct c, passato come parametro.
 */
void cleanup(struct floor_comm_struct *c);

#endif
