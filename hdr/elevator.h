#ifndef ELEVATOR_GUARD
#define ELEVATOR_GUARD

/*
 * Questa struttura rappresenta l'ascensore. Contiene una struct full_list,
 * memorizzata tramite un puntatore, che contiene le persone all'interno
 * dell'ascensore, un array di 4 interi, che rappresentano rispettivamente
 * il numero di bambini, adulti, addetti alla consegna e alla manutenzione
 * trasportati dall'inizio dell'esecuzione dell'applicazione, e due interi
 * senza segno: current_floor e current_weight. Il primo indica l'indice del
 * piano al quale l'ascensore si trova, mentre il secondo contiene il peso
 * totale, in chilogrammi, trasportato dall'ascensore.
 */
struct elevator
{
	struct full_list *passenger_list;
	unsigned int total_people[4];
	unsigned int current_floor;
	unsigned int current_weight;
};

/*
 * Questa struttura contiene le informazioni necessarie alla comunicazione
 * con i piani tramite socket. E' formata da un struct elevator, memorizzato tramite
 * puntatore, e un puntatore a FILE, ovvero il file di log su cui scrivere.
 */
struct comm_struct
{
	struct elevator *elevator;
	FILE *log_file;
};


/*
 * Questa funzione controlla che sia stato passato il numero corretto di
 * argomenti per l'esecuzione del programma. In caso negativo, stampa un
 * messaggio di errore e termina l'esecuzione
 */
void check_argument(int argc, char *name);

/*
 * Questa funzione crea una nuova istanza di struct elevator tramite malloc()
 * e restituisce il puntatore alla relativa area di memoria allocata.
 * Non riceve alcun parametro in ingresso.
 */
struct elevator *init_elevator(void);

/*
 * Questa funzione calcola la durata dell'applicazione in secondi in base
 * al parametro passato tramite linea di comando al momento del lancio
 * dell'applicazione
 */
int set_duration(char *argument);

/*
 * Questa funzione crea i file descriptor dei socket dei piani su cui
 * l'ascensore dovra' comunicare.
 */
void create_sockets(int sockets[4]);

/*
 * Questa funzione ha lo scopo di connettere il socket creato a quello di un
 * piano, tramite accept(). Sono creati quattro file descriptor distinti, uno
 * per piano, in modo da poter comunicare con essi indipendentemente.
 */
void connect_to_floor(struct sockaddr *socket_ptr, int elevator_socket,
		unsigned int length, int floor_sockets[4]);

/*
 * Questa funzione ha lo scopo di "bloccare" l'ascensore in attesa che tutti
 * e quattro i piani siano connessi. Sono eseguite, infatti, quattro read()
 * bloccanti sui quattro socket corrispondenti ai singoli piani.
 */
void wait_for_floors(int sockets[4]);

/*
 * Questa funzione crea una nuova istanza di struct comm_struct tramite malloc()
 * e restituisce il puntatore alla relativa area di memoria allocata.
 * I parametri, un puntatore a struct elevator e a FILE, servono a parametrizzare
 * la creazione della struttura con le istanze di struct elevator e FILE desiderate.
 */
struct comm_struct *init_comm_struct(struct elevator *e, FILE *f);


/*
 * Questa funzione simula il movimento dell'ascensore tra i piani. 
 * Il parametro di tipo struct elevator, passato tramite puntatore, serve a conoscere
 * le informazioni riguardanti il piano a cui l'ascensore si trova e la sua
 * prossima destinazione, mentre il parametro duration, intero, serve a
 * conoscere il numero di secondi che rimangono prima
 * del termine dell'applicazione.
 */
int move(struct elevator *e, int duration);

/*
 * Questa funzione, invocata da move(), simula il passaggio di tempo durante
 * il movimento dell'ascensore. Il parametro duration indica la durata totale
 * rimanente in secondi, mentre il parametro amount contiene il numero di
 * secondi che devono trascorrere.
 */
int spend_time(int duration, int amount);

/*
 * Questa funzione gestisce la comunicazione effettiva tra ascensore e piano.
 * L'ascensore, dopo aver "sbloccato" il processo piano corrispondente a
 * quello in cui l'ascensore si trova tramite una write sul socket
 * corrispondente, scarica le persone che avevano come destinazione tale piano,
 * e carica le persone in attesa.
 * (Naturalmente, entro il limite massimo di peso.)
 * Il parametro comm, di tipo struct comm_struct, passato tramite puntatore, contiene
 * le informazioni relative all'ascensore e al file di log da aggiornare,
 * mentre il parametro socket, intero, contiene il file descriptor del socket
 * da usare per la comunicazione.
 * (A ogni piano, infatti, corrisponde un diverso file descriptor.)
 */
void communicate(struct comm_struct *comm, int socket);

/*
 * Questa funzione gestisce lo scaricamento dell'ascensore, facendo scendere
 * le persone che hanno il piano corrente come destinazione. Il parametro f,
 * puntatore a FILE, contiene il file di log su cui scrivere, mentre e,
 * puntatore a struct elevator, e' usato per ottenere informazioni sul
 * piano corrente e sulle persone da scaricare.
 */
void deload_elevator(FILE *f, struct elevator *e);

/*
 * Questa funzione, invocata dalla precedente, deload_elevator(), rimuove
 * le persone da una singola sottolista contenuta nella struct full_list dell'ascensore.
 * il parametro f, puntatore a FILE, rappresenta il file di log su cui scrivere,
 * n, doppio puntatore a struct list_node, rappresenta un doppio puntatore alla testa
 * della sottolista da cui rimuovere persone.
 * (Il doppio puntatore e' necessario qualora si dovesse rimuovere il primo
 * elemento della lista: in tal caso, il puntatore dovrebbe essere modificato.)
 * Il parametro e, puntatore a struct elevator, serve per ottenere informazioni sul
 * piano corrente e sul peso, in modo da ridurlo a ogni scaricamento di una
 * persona.
 */
int deload_partial(FILE *f, struct list_node **head, struct elevator *e);

/*
 * Questa funzione legge la stringa inviata dal piano, contenente i dettagli
 * delle persone in attesa, e fa salire, fino al raggiungimento del limite di
 * peso consentito, nuovi passeggeri a bordo.
 * Il parametro f, puntatore a FILE, rappresenta il file di log su cui
 * scrivere, e, puntatore a struct elevator, serve per avere accesso alla lista
 * rappresentante le persone a bordo dell'ascensore e all'indice del
 * piano corrente, mentre socket, intero, contiene il file descriptor del
 * socket su cui comunicare.
 */
void load_people(FILE *f, struct elevator *e, int socket);

/*
 * Questa funzione, invocata da load_people(), legge i dettagli di una singola
 * persona dalla stringa passata sul socket. Restituisce 1 se una persona e'
 * stata letta con successo, altrimenti, se si e' arrivati alla fine della
 * stringa, restituisce 0.
 */
int read_single_person(struct person *person, int socket);

/*
 * Questa funzione aumenta il numero di persone totali trasportate dall'inizio
 * dell'esecuzione. Il parametro p, di tipo struct person, serve a conoscere il tipo
 * di persona salita a bordo, mentre total, array di quattro interi,
 * rappresenta il numero totale di passeggeri per ogni categoria di persona.
 * (In ordine: bambini, adulti, addetti alla consegna, addetti alla manutenzione.)
 */
void increase_total_people(struct person p, unsigned int total[4]);

/*
 * Questa funzione dealloca la memoria rimanente sull'heap allocata dal puntatore
 * a struct comm_struct passato come parametro.
 */
void cleanup(struct comm_struct *c);

/*
 * Questa funzione comunica a tutti i piani la terminazione del programma,
 * in modo che anche tali processi possano terminare normalmente.
 * Poiche' essi si mettono in attesa di leggere un messaggio di due caratteri,
 * e' stato scelto di scrivere, in maniera puramente arbitraria, la stringa
 * "no" su ognuno dei quattro socket, per comunicare la terminazione.
 */
void kill_floors(int sockets[4]);

#endif
