#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../hdr/types.h"
#include "../hdr/log.h"

/*
 * Questa funzione restituisce l'ora corrente.
 */
static char *get_time(char *current_time);


void floor_log_update(FILE *f, struct person p)
{
	char curr_time[30] = "";
	char *person_type = "";

	get_time(curr_time);
	switch (p.weight) {
	case CHILD_WEIGHT:
		person_type = "bambino";
		break;
	case ADULT_WEIGHT:
		person_type = "adulto";
		break;
	case CARRIER_WEIGHT:
		person_type = "addetto alla consegna";
		break;
	case MAINT_WEIGHT:
		person_type = "addetto alla manutenzione";
	}
	fprintf(f, "[GENERATO], %s, %s, %s, destinazione=%d\n", person_type, \
	        __DATE__, curr_time, p.destination);
}

void stop_log_update(FILE *f, int loc)
{
	char curr_time[30] = "";

	get_time(curr_time);
	fprintf(f, "\n[FERMATA] piano %d, %s, %s\n", loc,
	        __DATE__, curr_time);
}

void load_log_update(FILE *f, struct person p, int loc)
{
	char curr_time[30] = "";
	char *person_type = "";

	get_time(curr_time);
	switch (p.weight) {
	case CHILD_WEIGHT:
		person_type = "bambino";
		break;
	case ADULT_WEIGHT:
		person_type = "adulto";
		break;
	case CARRIER_WEIGHT:
		person_type = "addetto alla consegna";
		break;
	case MAINT_WEIGHT:
		person_type = "addetto alla manutenzione";
	}
	fprintf(f, "[SALITO A BORDO] %s, %s, %s, piano %d, destinazione %d\n", \
	        person_type, __DATE__, curr_time, loc, p.destination);
}

void deload_log_update(FILE *f, enum weight weight, int loc)
{
	char curr_time[30] = "";
	char *person_type = "";

	get_time(curr_time);
	switch (weight) {
	case CHILD_WEIGHT:
		person_type = "bambino";
		break;
	case ADULT_WEIGHT:
		person_type = "adulto";
		break;
	case CARRIER_WEIGHT:
		person_type = "addetto alla consegna";
		break;
	case MAINT_WEIGHT:
		person_type = "addetto alla manutenzione";
	}
	fprintf(f, "[SCESO] %s, %s, %s, piano %d\n", person_type, __DATE__, \
	        curr_time, loc);
}

static char *get_time(char *current_time)
{
	time_t time_in_seconds = time(NULL);
	struct tm *full_time = localtime(&time_in_seconds);
	sprintf(current_time, "%.2d:%.2d:%.2d", full_time->tm_hour, full_time->tm_min,
	        full_time->tm_sec);
	return current_time;
}

void final_log_print(FILE *f, unsigned int people[4])
{
	fputs("\nTerminazione esecuzione!\n", f);
	fputs("Riassunto dell'attivita' odierna:\n", f);
	fprintf(f, "Numero bambini trasportati=%d\n", people[0]);
	fprintf(f, "Numero adulti trasportati=%d\n", people[1]);
	fprintf(f, "Numero addetti alla consegna trasportati=%d\n", people[2]);
	fprintf(f, "Numero addetti alla manutenzione trasportati=%d\n\n", people[3]);
}
