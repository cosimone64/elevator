#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <semaphore.h>
#include <time.h>
#include "../hdr/types.h"
#include "../hdr/operations.h"

struct person init_person(enum weight w, int destination)
{
	struct person tmp_person;

	tmp_person.weight = w;
	tmp_person.destination = destination;
	return tmp_person;
}

int randomize_destination(void)
{
	srand(time(NULL));
	return rand() % 4;
}

int safe_read(int fd, char *buffer, int count)
{
	int chars_read = read(fd, buffer, count);

	if (chars_read < 0) {
		perror("Error during read");
		exit(EXIT_FAILURE);
	}
	return chars_read;
}

int safe_write(int fd, char *buffer, int count)
{
	int chars_written = write(fd, buffer, count);

	if (chars_written < 0) {
		perror("Error during write");
		exit(EXIT_FAILURE);
	}
	return chars_written;
}
