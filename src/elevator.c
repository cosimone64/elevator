#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include "../hdr/macros.h"
#include "../hdr/types.h"
#include "../hdr/operations.h"
#include "../hdr/list.h"
#include "../hdr/log.h"
#include "../hdr/elevator.h"

#define SOCKET_NUMBER 4
#define STOP_INTERVAL 3

int main(int argc, char *argv[])
{
	struct elevator *elevator = init_elevator();
	FILE *elevator_log = fopen("../log/elevatorlog.txt", "w");
	struct comm_struct *comm = init_comm_struct(elevator, elevator_log);
	int floor_sockets[4];

	check_argument(argc, argv[0]);
	create_sockets(floor_sockets);
	wait_for_floors(floor_sockets);
	int duration = set_duration(argv[1]);
	while (duration > 0) {
		sleep(3);
		duration = duration - 3;
		communicate(comm, floor_sockets[elevator->current_floor]);
		duration = move(elevator, duration);
		printf("L'ascensore sta trasportando %d Kg\n",
		       elevator->current_weight);
		stop_log_update(elevator_log, elevator->current_floor);
		stop_log_update(stdout, elevator->current_floor);
	}
	final_log_print(stdout, elevator->total_people);
	final_log_print(elevator_log, elevator->total_people);
	unlink(SOCKET_NAME);
	cleanup(comm);
	kill_floors(floor_sockets);
	return 0;
}

void check_argument(int argc, char *name)
{
	if (argc != 2) {
		fprintf(stderr, "Avviare come %s durataapplicazione=n\n",
		        name);
		exit(EXIT_FAILURE);
	}
}

struct elevator *init_elevator(void)
{
	struct elevator *elevator_ptr = calloc(1, sizeof(struct elevator));

	elevator_ptr->passenger_list = init_list();
	return elevator_ptr;
}

int set_duration(char *argument)
{
	char full_argument[64] = "";

	strcpy(full_argument, argument);
	strtok(argument, "=");
	char *string_to_remove = strcat(argument, "=");
	if (strcmp(string_to_remove, CORRECT_ARGUMENT)) {
		perror("Wrong argument format!");
		exit(EXIT_FAILURE);
	}
	char *string_duration = strstr(full_argument, string_to_remove);
	if (string_duration == NULL) {
		perror("Wrong Argument format!");
		exit(EXIT_FAILURE);
	}
	strcpy(string_duration, string_duration + strlen(string_to_remove));
	return (atoi(string_duration) * 60);
}

void create_sockets(int sockets[4])
{
	unsigned int server_length = sizeof(struct sockaddr_un);
	struct sockaddr_un elevator_sock_addr, floor_sock_addr;
	int elevator_socket = socket(AF_UNIX, SOCK_STREAM, DEFAULT_PROTOCOL);

	if (elevator_socket < 0) {
		perror("Error creating socket");
		exit(EXIT_FAILURE);
	}
	struct sockaddr *elevator_sock_ptr =
	        (struct sockaddr *) &elevator_sock_addr;
	struct sockaddr *floor_sock_ptr = (struct sockaddr *) &floor_sock_addr;
	elevator_sock_addr.sun_family = AF_UNIX;
	strcpy(elevator_sock_addr.sun_path, SOCKET_NAME);
	unlink(SOCKET_NAME);
	if (bind(elevator_socket, elevator_sock_ptr, server_length)) {
		unlink(SOCKET_NAME);
		perror("Error creating socket");
		exit(EXIT_FAILURE);
	}
	listen(elevator_socket, 4);
	for (int i = 0; i < 4; i++)
		connect_to_floor(floor_sock_ptr, elevator_socket,
		                 server_length, sockets);
	close(elevator_socket);
}

void connect_to_floor(struct sockaddr *socket_ptr, int elevator_socket,
                      unsigned int length, int sockets[4])
{
	char buffer[8] = "";
	int tmp_socket = accept(elevator_socket, socket_ptr, &length);

	if (tmp_socket < 0) {
		perror("Accepting connection failed");
		exit(EXIT_FAILURE);
	}
	safe_read(tmp_socket, buffer, 1);
	int floor_number = atoi(buffer);
	sockets[floor_number] = tmp_socket;
	printf("Connesso al piano %d\n\n", floor_number);
}

void wait_for_floors(int sockets[4])
{
	char buffer[8] = "";

	for (int i = 0; i < 4; i++) {
		safe_read(sockets[i], buffer, 3);
		safe_write(sockets[i], buffer, 3);
	}
}

struct comm_struct *init_comm_struct(struct elevator *e, FILE *f)
{
	struct comm_struct *tmp = malloc(sizeof(struct comm_struct));

	tmp->elevator = e;
	tmp->log_file = f;
	return tmp;
}

void communicate(struct comm_struct *comm, int socket)
{
	struct elevator *elevator = comm->elevator;
	FILE *log_file = comm->log_file;

	safe_write(socket, "ok", 2);
	deload_elevator(log_file, elevator);
	load_people(log_file, elevator, socket);
}

void deload_elevator(FILE *f, struct elevator *e)
{
	struct list_node **normal_list = &(e->passenger_list->normal_list);
	struct list_node **priority_list = &(e->passenger_list->priority_list);
	unsigned int *nodes_in_list = &(e->passenger_list->number_of_nodes);
	unsigned int removed_people = deload_partial(f, priority_list, e);

	removed_people = removed_people + deload_partial(f, normal_list, e);
	*nodes_in_list -= *nodes_in_list - removed_people;
}
efef

int deload_partial(FILE *f, struct list_node **head, struct elevator *e)
{
	struct list_node *current_node = *head;
	unsigned int current_floor = e->current_floor;
	unsigned int *current_weight = &(e->current_weight);
	unsigned int removed_people = 0;

	while (current_node) {
		struct list_node *next_node = current_node->next;
		struct person removed_person;
		if (current_node->person.destination == current_floor) {
			deload_log_update(stdout, current_node->person.weight,
			                  current_floor);
			deload_log_update(f, current_node->person.weight,
			                  current_floor);
			removed_person = remove_local(current_node, head);
			removed_people++;
			*current_weight =
			        (*current_weight) - removed_person.weight;
		}
		current_node = next_node;
	}
	return removed_people;
}

void load_people(FILE *f, struct elevator *e, int socket)
{
	unsigned int *current_weight = &(e->current_weight);
	unsigned int removed_people = 0;
	char buffer[8] = "";
	struct person person;

	while (read_single_person(&person, socket)) {
		if (person.weight <= MAX_WEIGHT - (*current_weight)) {
			insert_last(e->passenger_list, person);
			*current_weight = (*current_weight) + person.weight;
			increase_total_people(person, (e->total_people));
			removed_people++;
			load_log_update(stdout, person, e->current_floor);
			load_log_update(f, person, e->current_floor);
		}
	}
	sprintf(buffer, "%d ", removed_people);
	safe_write(socket, buffer, 2);
}

int read_single_person(struct person *person, int socket)
{
	char buffer[32] = "";

	safe_read(socket, buffer, 6);
	int tmp_weight = atoi(strtok(buffer, " "));
	if (tmp_weight < 0)
		return 0;
	int tmp_destination = atoi(strtok(NULL, " "));
	*person = init_person(tmp_weight, tmp_destination);
	return 1;
}

void increase_total_people(struct person p, unsigned int total[4])
{
	switch (p.weight) {
	case CHILD_WEIGHT:
		total[0]++;
		break;
	case ADULT_WEIGHT:
		total[1]++;
		break;
	case CARRIER_WEIGHT:
		total[2]++;
		break;
	case MAINT_WEIGHT:
		total[3]++;
	}
}

int move(struct elevator *e, int duration)
{
	struct person *tmp_person_ptr;
	int destination = 0;

	if (duration <= 0)
		return 0;
	tmp_person_ptr = peek_list(e->passenger_list);
	int current_floor = e->current_floor;
	if (tmp_person_ptr)
		destination = tmp_person_ptr->destination;
	else
		destination = (current_floor + 1) % 4;
	int time_to_sleep = 3 * abs(destination - current_floor);
	e->current_floor = destination;
	return spend_time(duration, time_to_sleep);
}

int spend_time(int duration, int amount)
{
	if (amount <= duration) {
		sleep(amount);
		return (duration - amount);
	} else {
		sleep(duration);
		return 0;
	}
}

void cleanup(struct comm_struct *c)
{
	struct elevator *e = c->elevator;

	destroy_list(e->passenger_list);
	free(e);
	fclose(c->log_file);
	free(c);
}

void kill_floors(int sockets[4])
{
	for (int i = 0; i < 4; ++i) {
		safe_write(sockets[i], "no", 2);
		close(sockets[i]);
	}
}
