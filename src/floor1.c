#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include "../hdr/macros.h"
#include "../hdr/types.h"
#include "../hdr/list.h"
#include "../hdr/floor.h"
#include "../hdr/operations.h"
#include "../hdr/log.h"

/*
 * Funzione main del piano. Dopo aver dichiarato la struttura di
 * comunicazione, il puntatore alla lista di attesa, e aperto il file di log,
 * viene anche dichiarato un nuovo thread, assieme al semaforo
 * di sincronizzazione.
 *
 * Il file descriptor su cui comunicare viene ottenuto tramite la funzione
 * create_floor_socket(). Un array di sei interi, params, contiene, in ordine:
 * le quattro frequenze di arrivo delle diverse tipologie di persone,
 * (bambini, adulti, addetti alla consegna e addetti alla manutenzione)
 * il numero identificativo del piano, e il file descriptor del socket.
 *
 * Dopo aver inizializzato il semaforo, si aspetta che il processo ascensore
 * acconsenta di proseguire. La struttura di comunicazione viene quindi
 * inizializzata.
 *
 * Il thread generatore viene in seguito creato e distaccato dal thread
 * principale. A questo punto, il piano comunica con l'ascensore finche'
 * non riceve da quest'ultimo il segnale di terminazione esecuzione.
 *
 * Dopo aver chiuso il file di log, si esce dal programma.
 */
int main(void)
{
	struct full_list *list = init_list();
	FILE *log_file = fopen("../log/floor1log.txt", "w");
	int socket = create_floor_socket();
	int params[] = {
		CHILD_FREQ_F1, ADULT_FREQ_F1, CARRIER_FREQ_F1,
		MAINT_FREQ_F1, 1, socket
	};
	pthread_t generator_thread;
	sem_t list_semaphore;

	safe_sem_init(&list_semaphore, 0, 1);
	wait_for_elevator(socket, 1);
	struct floor_comm_struct *comm_struct = init_comm_struct(list,
	                                        &list_semaphore, params, log_file);
	safe_pthread_create(&generator_thread, NULL,
	                    generate_people, (void *) comm_struct);
	safe_pthread_detach(generator_thread);

	while (communicate_with_elevator(comm_struct))
		sleep(1);
	destroy_list(list);
	fclose(log_file);
	free(comm_struct);
	return 0;
}
