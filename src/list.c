#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "../hdr/types.h"
#include "../hdr/list.h"


/*
 * Questa funzione rimuove dalla sottolista n, di tipo struct list_node, passata
 * tramite doppio puntatore, la prima persona della lista.
 * E' invocata da remove_first(). Il doppio puntatore e' necessario per
 * modificare eventualmente il puntatore al primo elemento della lista.
 */
static struct person remove_partial_list(struct list_node **n);

/*
 * Questa funzione inserisce effettivamente la persona p, passata come
 * parametro, nella sottolista n, passata tramite doppio puntatore.
 * E' invocata da insert_last(), che passa la sottolista opportuna in cui
 * aggiungere la persona. Il doppio puntatore e' necessario per cambiare
 * il riferimento, qualora dovessimo inserire un nuovo elemento in una lista
 * vuota.
 */
static void insert_partial_list(struct list_node **n, struct person p);

/*
 * Questa funzione crea una nuova istanza di struct list_node. E' invocata da
 * insert_last(), qualora la sottolista in cui inserire un nodo fosse vuota.
 * Riceve come parametro una persona p, di tipo struct person, per inserirla
 * nella lista. E' invocata da insert_partial_list().
 */
static struct list_node *init_node(struct person p);

/*
 * Questa funzione restituisce l'ultimo elemento di una sottolista
 * n, di tipo struct list_node, passata tramite puntatore.
 * Cicla finche' non trova un puntatore a NULL nel campo next del nodo.
 * E' invocata da insert_partial_list().
 */
static struct list_node *get_last_node(struct list_node *n);

/*
 * Questa funzione dealloca tutta l'area di memoria allocata dalla sottolista
 * puntata dal parametro n, puntatore a struct list_node.
 */
static void destroy_partial(struct list_node *n);


struct full_list *init_list(void)
{
	return (struct full_list *) calloc(1, sizeof(struct full_list));
}

struct person remove_first(struct full_list *l)
{
	l->number_of_nodes--;
	if (l->priority_list)
		return remove_partial_list(&(l->priority_list));
	else if (l->normal_list)
		return remove_partial_list(&(l->normal_list));
	perror("Removal from empty list");
	exit(EXIT_FAILURE);
}

static struct person remove_partial_list(struct list_node **n)
{
	struct person tmp_person = (*n)->person;
	struct list_node *tmp_node = (*n)->next;

	free(*n);
	if (tmp_node)
		tmp_node->previous = NULL;
	*n = tmp_node;
	return tmp_person;
}

struct person *peek_list(struct full_list *l)
{
	if (l->priority_list)
		return  &(l->priority_list->person);
	else if (l->normal_list)
		return  &(l->normal_list->person);
	return NULL;
}

struct person remove_local(struct list_node *n, struct list_node **head)
{
	if (!(*head)) {
		perror("Removal from empty list");
		exit(EXIT_FAILURE);
	}
	struct person tmp_person = n->person;
	struct list_node *tmp_next = n->next;
	struct list_node *tmp_previous = n->previous;
	if (*head == n)
		*head = (*head)->next;
	if (tmp_next)
		tmp_next->previous = n->previous;
	if (tmp_previous)
		tmp_previous->next = n->next;
	free(n);
	return tmp_person;
}

void insert_last(struct full_list *l, struct person p)
{
	if (l->number_of_nodes < 40) {
		l->number_of_nodes++;
		if (p.weight == MAINT_WEIGHT)
			insert_partial_list(&(l->priority_list), p);
		else
			insert_partial_list(&(l->normal_list), p);
	}
}

static void insert_partial_list(struct list_node **n, struct person p)
{
	struct list_node *new_node = init_node(p);

	if (!(*n)) {
		*n = new_node;
		return;
	}
	struct list_node *old_last_node = get_last_node(*n);
	old_last_node->next = new_node;
	new_node->previous = old_last_node;
}

static struct list_node *init_node(struct person p)
{
	struct list_node *tmp_node = calloc(1, sizeof(struct list_node));

	tmp_node->person = p;
	return tmp_node;
}

static struct list_node *get_last_node(struct list_node *n)
{
	while (n->next)
		n = n->next;
	return n;
}

void destroy_list(struct full_list *l)
{
	destroy_partial(l->priority_list);
	destroy_partial(l->normal_list);
	free(l);
}

static void destroy_partial(struct list_node *n)
{
	if (!n) return;

	while(n->next != NULL) {
		n = n->next;
		free(n->previous);
	}
	free(n);
}
