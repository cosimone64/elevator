#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <pthread.h>
#include <semaphore.h>
#include "../hdr/macros.h"
#include "../hdr/types.h"
#include "../hdr/list.h"
#include "../hdr/floor.h"
#include "../hdr/operations.h"
#include "../hdr/log.h"

/*
 * Questa funzione crea la stringa contenente i dettagli delle persone da
 * passare all'ascensore. Si leggono le persone dalla lista l, passata tramite
 * puntatore, e, infine, la stringa viene scritta sul socket passato come
 * secondo parametro. E' invocata da communicate_with_elevator().
 */
static void create_message(struct full_list *l, int socket);

/*
 * Questa funzione, invocata da create_message(), scrive i dettagli delle
 * persone contenute nella sottolista n, passata come parametro tramite
 * puntatore. Prima si legge la lista degli addetti alla manutenzione,
 * dopodiche' la lista contenente le altre persone, con una nuova invocazione.
 */
static void create_partial_message(struct list_node *n, char message[]);

/*
 * Questa funzione rimuove dalla lista l un numero di persone pari al
 * numero indicato da people_to_remove.
 * E' invocata da communicate_with_elevator().
 */
static void remove_people(struct full_list *l, int people_to_remove);

/*
 * Questa funzione restituisce una destinazione per la persona appena
 * creata. Il parametro in input, floor_number, serve a non generare
 * una persona che abbia come destinazione lo stesso piano in cui e'
 * stata generata. E' invocata da generate_people().
 */
static int get_destination(unsigned int floor_number);

/*
 * Questa funzione inserisce la persona p passata come parametro nella lista
 * l, passata tramite puntatore. Il semaforo passato serve a sincronizzare il
 * thread generatore con il thread principale del piano.
 * E' invocata da generate_people().
 */
static void synchronized_insert(struct full_list *l, struct person p,
                                sem_t *semaphore);


struct floor_comm_struct *init_comm_struct(struct full_list *list,
                sem_t *semaphore, int int_params[6], FILE *file)
{
	struct floor_comm_struct *comm = malloc(sizeof(struct floor_comm_struct));

	comm->list = list;
	for (int i = 0; i < 4; ++i)
		comm->freqs[i] = int_params[i];
	comm->file = file;
	comm->semaphore = semaphore;
	comm->floor_number = int_params[4];
	comm->socket = int_params[5];
	return comm;
}

int create_floor_socket(void)
{
	unsigned int server_length = sizeof(struct sockaddr_un);
	int floor_socket = socket(AF_UNIX, SOCK_STREAM, DEFAULT_PROTOCOL);

	if (floor_socket < 0) {
		perror("Error creating Socket!");
		exit(EXIT_FAILURE);
	}
	struct sockaddr_un server;
	struct sockaddr *sock_addr_ptr = (struct sockaddr *) &server;
	server.sun_family = AF_UNIX;
	strcpy(server.sun_path, SOCKET_NAME);
	for (int conn_result = -1; conn_result < 0;) {
		conn_result = connect(floor_socket, sock_addr_ptr, server_length);
		sleep(1);
	}
	return floor_socket;
}

void wait_for_elevator(int socket, int floor_number)
{
	char buffer[8] = "";

	sprintf(buffer, "%d", floor_number);
	safe_write(socket, buffer, 1);

	sprintf(buffer, "ok!");
	safe_write(socket, buffer, 3);

	safe_read(socket, buffer, 3);
	printf("Connessione accettata dall'ascensore\n");
}

int communicate_with_elevator(struct floor_comm_struct *params)
{
	struct full_list *list = params->list;
	sem_t *semaphore = params->semaphore;
	int socket = params->socket;
	char ok_to_go_buffer[8] = "";

	safe_read(socket, ok_to_go_buffer, 2);
	if (!strcmp(ok_to_go_buffer, "no"))
		return 0;
	safe_sem_wait(semaphore);
	create_message(list, socket);
	remove_people(list, socket);
	safe_sem_post(semaphore);
	return 1;
}

static void create_message(struct full_list *l, int socket)
{
	struct list_node *normal_list = l->normal_list;
	struct list_node *priority_list = l->priority_list;
	char buffer[1024] = "";

	create_partial_message(priority_list, buffer);
	create_partial_message(normal_list, buffer);
	strcat(buffer, "-1    ");
	safe_write(socket, buffer, strlen(buffer));
}

static void create_partial_message(struct list_node *n, char message[])
{
	char person_params[16] = "";

	while (n) {
		struct person person = n->person;
		sprintf(person_params, "%3d %d ", person.weight,
		        person.destination);
		strcat(message, person_params);
		n = n->next;
	}
}

static void remove_people(struct full_list *l, int socket)
{
	char buffer[8] = "";

	safe_read(socket, buffer, 2);
	int people_to_remove = atoi(buffer);
	for (int i = 0; i < people_to_remove; ++i)
		remove_first(l);
}

void *generate_people(void *params)
{
	struct floor_comm_struct *comm_params = (struct floor_comm_struct *) params;
	struct full_list *list = comm_params->list;
	sem_t *semaphore = comm_params->semaphore;
	FILE *log_file = comm_params->file;
	int child_freq = comm_params->freqs[0];
	int adult_freq = comm_params->freqs[1];
	int carrier_freq = comm_params->freqs[2];
	int maint_freq = comm_params->freqs[3];
	int floor_number = comm_params->floor_number;
	int current_time = 0;

	while(1) {
		sleep(5);
		current_time = (current_time + 5);
		if (!(current_time % child_freq)) {
			struct person tmp_person = init_person(CHILD_WEIGHT,
			                                       get_destination(floor_number));
			synchronized_insert(list, tmp_person, semaphore);
			floor_log_update(stdout, tmp_person);
			floor_log_update(log_file, tmp_person);
		}
		if (!(current_time % adult_freq)) {
			struct person tmp_person = init_person(ADULT_WEIGHT,
			                                       get_destination(floor_number));
			synchronized_insert(list, tmp_person, semaphore);
			floor_log_update(stdout, tmp_person);
			floor_log_update(log_file, tmp_person);
		}
		if (!(current_time % carrier_freq)) {
			struct person tmp_person = init_person(CARRIER_WEIGHT,
			                                       get_destination(floor_number));
			synchronized_insert(list, tmp_person, semaphore);
			floor_log_update(stdout, tmp_person);
			floor_log_update(log_file, tmp_person);
		}
		if (!(current_time % maint_freq)) {
			struct person tmp_person = init_person(MAINT_WEIGHT,
			                                       get_destination(floor_number));
			synchronized_insert(list, tmp_person, semaphore);
			floor_log_update(stdout, tmp_person);
			floor_log_update(log_file, tmp_person);
		}
	}
	return NULL;
}

static void synchronized_insert(struct full_list *l, struct person p,
                                sem_t *semaphore)
{
	safe_sem_wait(semaphore);
	insert_last(l, p);
	safe_sem_post(semaphore);
}

void safe_sem_wait(sem_t *semaphore)
{
	if (sem_wait(semaphore) < 0) {
		perror("Error waiting on semaphore");
		exit(EXIT_FAILURE);
	}
}

void safe_sem_post(sem_t *semaphore)
{
	if (sem_post(semaphore) < 0) {
		perror("Error releasing semaphore");
		exit(EXIT_FAILURE);
	}
}

static int get_destination(unsigned int floor_number)
{
	unsigned int result = floor_number;

	while (result == floor_number)
		result = randomize_destination();
	return result;
}

int safe_pthread_create(pthread_t *thread, const pthread_attr_t *attr,
                        void *(*start_routine) (void *), void *arg)
{
	int result = pthread_create(thread, attr, start_routine, arg);

	if (result) {
		perror("Error creating thread");
		exit(EXIT_FAILURE);
	}
	return result;
}

int safe_pthread_detach(pthread_t thread)
{
	int result = pthread_detach(thread);

	if (result) {
		perror("Error detaching thread");
		exit(EXIT_FAILURE);
	}
	return result;
}

void safe_sem_init(sem_t *sem, int pshared, unsigned int value)
{
	if (sem_init(sem, pshared, value) < 0) {
		perror("Error initializing semaphore");
		exit(EXIT_FAILURE);
	}
}

void cleanup(struct floor_comm_struct *c)
{
	destroy_list(c->list);
	fclose(c->file);
	free(c->semaphore);
	free(c);
}
