VPATH = ./obj:./src:./hdr:
CC = gcc
CFLAGS = -std=c11
LIBS = -lpthread

all: obj ./bin/elevator ./bin/floor0 ./bin/floor1 ./bin/floor2 ./bin/floor3

obj: ./obj/elevator.o ./obj/floor0.o ./obj/floor1.o ./obj/floor2.o \
	./obj/floor3.o ./obj/floor.o ./obj/list.o ./obj/log.o ./obj/operations.o

clean:
	rm -rf ./obj/*.o
	rm -rf ./bin/*
	rm -rf ./log/*log.txt


./bin/elevator: elevator.o list.o log.o operations.o
	$(CC) $(CFLAGS) -o ./bin/elevator ./obj/elevator.o ./obj/list.o ./obj/log.o\
		./obj/operations.o

./bin/floor0: floor0.o floor.o list.o log.o operations.o
	$(CC) $(CFLAGS) -o ./bin/floor0 ./obj/floor0.o ./obj/floor.o ./obj/list.o\
		./obj/log.o ./obj/operations.o $(LIBS)

./bin/floor1: floor1.o floor.o list.o log.o operations.o
	$(CC) $(CFLAGS) -o ./bin/floor1 ./obj/floor1.o ./obj/floor.o ./obj/list.o\
		./obj/log.o ./obj/operations.o $(LIBS)

./bin/floor2: floor2.o floor.o list.o log.o operations.o
	$(CC) $(CFLAGS) -o ./bin/floor2 ./obj/floor2.o ./obj/floor.o ./obj/list.o\
		./obj/log.o ./obj/operations.o $(LIBS)

./bin/floor3: floor3.o floor.o list.o log.o operations.o
	$(CC) $(CFLAGS) -o ./bin/floor3 ./obj/floor3.o ./obj/floor.o ./obj/list.o\
		./obj/log.o ./obj/operations.o $(LIBS)


./obj/elevator.o: elevator.c macros.h types.h operations.h\
	list.h log.h elevator.h
	$(CC) $(CFLAGS) -c -o ./obj/elevator.o ./src/elevator.c

./obj/floor0.o: floor0.c macros.h types.h floor.h\
	list.h operations.h log.h
	$(CC) $(CFLAGS) -c -o ./obj/floor0.o ./src/floor0.c

./obj/floor1.o: floor1.c macros.h types.h floor.h\
	list.h operations.h log.h
	$(CC) $(CFLAGS) -c -o ./obj/floor1.o ./src/floor1.c

./obj/floor2.o: floor0.c macros.h types.h floor.h\
	list.h operations.h log.h
	$(CC) $(CFLAGS) -c -o ./obj/floor2.o ./src/floor2.c

./obj/floor3.o: floor3.c macros.h types.h floor.h\
	list.h operations.h log.h
	$(CC) $(CFLAGS) -c -o ./obj/floor3.o ./src/floor3.c

./obj/floor.o: floor.c macros.h types.h floor.h\
	list.h operations.h log.h
	$(CC) $(CFLAGS) -c -o ./obj/floor.o ./src/floor.c

./obj/list.o: list.c types.h list.h
	$(CC) $(CFLAGS) -c -o ./obj/list.o ./src/list.c

./obj/log.o: log.c types.h log.h
	$(CC) $(CFLAGS) -c -o ./obj/log.o ./src/log.c

./obj/operations.o: operations.c types.h operations.h
	$(CC) $(CFLAGS) -c -o ./obj/operations.o ./src/operations.c
